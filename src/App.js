import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider,  createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import Layout from './components/Layout/Layout';
import Recognition from './containers/Recognition/Recognition';
const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});

class App extends Component {
  render() {
    return (
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
              <Layout>
                <Switch>
                    <Route exact path="/" component={Recognition}/>
                </Switch>
              </Layout>
      </MuiThemeProvider>
    </BrowserRouter>
    );

  }
}

export default App;
