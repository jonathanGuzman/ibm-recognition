import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import RecognitionUpload from '../../components/RecognitionUpload/RecognitionUpload'
import axios from '../../axios.config';
import Grid from '@material-ui/core/Grid';
import RecognitionImg from '../../components/RecognitionImg/RecognitionImg'
import InfoTable from '../../components/InfoTable/InfoTable';
import nophoto from '../../images/images.png'
const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  wrapperContainer: {
      marginTop: '64px  '
  },
  media: {
      width: '200px',
      height: '200px'
  }
});
class Recognition extends Component {
    state = {
        uploadImage: false,
        pathFile: nophoto,
        faces : [],
        source: null,
    }
    UploadFileHandler = ( event ) =>{
        let file = event.target.files[0];
        console.log(file);
        
        if (file) {
        var newUpload = new FormData();
        newUpload.append('fileFace', file);
        axios.post('/images', newUpload )
        .then((resp) => {
            console.log(resp);
            this.setState({
                faces: resp.data.images[0].faces,
                pathFile: resp.data.images[0].resolved_url
                });
        })
        .catch((err) => {
            console.log(err);    
        })
        }
    }
    render() {
        const {classes} = this.props;
        return (
            <Grid 
                container 
                className={classes.wrapperContainer}
            >
                <Grid 
                    container 
                    direction="row"
                    justify="center"
                    alignItems="center"
                    className={classes.wrapperContainer}
                >
                    <Grid item xs={12}>
                        <InfoTable faces={this.state.faces}/>
                    </Grid> 
                    <Grid item xs={12}>
                    
                    <Grid 
                            container 
                            direction="column"
                            justify="center"
                            alignItems="center"
                            className={classes.wrapperContainer}
                        >
                        <RecognitionImg 
                            imageSrc={this.state.pathFile}
                            faces={this.state.faces}
                        />
                        <RecognitionUpload 
                            changed={this.UploadFileHandler} 
                            uploadImage={this.state.uploadImage}
                        />
                    </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles)(Recognition);