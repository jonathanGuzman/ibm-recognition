module.exports = {
    BUCKET_NAME: process.env.BUCKET_NAME || 'jonathanfazdemos',
    PROJECT_ID: process.env.PROJECT_ID || 'Project-demo',
    IBM: {
      API_KEY:  process.env.API_KEY || 'c403fa79b6aeab872f0fb51d72857497b821a3d7',//fake api key
      VERSION: '2018-03-19',
    },
    PORT: process.env.PORT || 5000
};
  