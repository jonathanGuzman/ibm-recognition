const express = require('express');
const bodyParser = require('body-parser');
const VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');
const path = require('path');
const mime = require('mime');
const multer = require('multer');
const crypto = require('crypto');
const cors = require('cors');
const CONFIG = require('./config/config');
const {Storage} = require('@google-cloud/storage');
//variables
const BUCKET_NAME = CONFIG.BUCKET_NAME;
//Express instance
const app = express()
//Google Storage
const storage = new Storage({
   projectId: CONFIG.PROJECT_ID,
   keyFilename: path.join(__dirname, 'key', 'keyfile.json')
});
//multer config
const multer_storage = multer.diskStorage({
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
    });
  }
});
//multer middleware
const upload = multer({storage : multer_storage}); 

app.use(bodyParser.json());

const visualRecognition = new VisualRecognitionV3({
  iam_apikey: CONFIG.IBM.API_KEY,
  version: CONFIG.IBM.VERSION
});

app.use(cors());
// serve static files
app.use('/static', express.static(path.join(__dirname, '../build', 'static')));

app.post('/images', upload.single('fileFace'), (req, res) => {
  if (!req.file) {
    res.status(500).send('No file received.');
  } else {
    const filename = req.file.filename;
    storage
    .bucket(BUCKET_NAME)
    .upload(req.file.path)
    .then((resp) => {
      const url_file = `https://${BUCKET_NAME}.storage.googleapis.com/${filename}`;
      console.log(`${filename} uploaded to ${BUCKET_NAME}.`);
      const params = {
          url: url_file
      };
      visualRecognition.detectFaces(params, function(err, response) {
        if (err){
          console.log(err);
          res.json(err);
        }else{
          res.json(response);
        }
      });
    })

  }
    
})


app.get('*', (req, res)=>{
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});
app.listen(CONFIG.PORT, () => console.log('Example app listening on port 5000!'))