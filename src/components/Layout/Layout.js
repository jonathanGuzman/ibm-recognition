import React, { Component, Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Link } from 'react-router-dom';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
//icons
import MenuIcon from '@material-ui/icons/Menu';
import NotesIcon from '@material-ui/icons/Notes';
import HomeIcon from '@material-ui/icons/Home';
const styles = theme => ({
  drawerGrid: {
    width: 250,
  },
  footer: {
    height: "30vh",
  },
  footerLinks: {
    margin: "10px"
  },
  alignTitle:{
    flexGrow: 1,
    textAlign: "left"
  }
});

class Layout extends Component {
    constructor() {
        super();
        this.state = {
            isOpen: false,
            openLogin: false
        };
    }
    toggleDrawer = (open) => () => {
        this.setState({
            isOpen: open
        });
    };
    handleOpen = ()=> {
        this.setState({ openLogin: true });
    }
    handleClose = () => {
        this.setState({ openLogin: false });
    };
    closeDialog = () => () => {
        this.setState({
            openLogin: false
        });
    };

    DrawerOptions = () => {
        return(     
            <div style={{width: "100%"}}>
                <List component="nav">
                    <ListItem button>
                        <ListItemIcon>
                            <NotesIcon />
                        </ListItemIcon>
                        <ListItemText primary="Recognition" />
                    </ListItem >           
                </List>
            </div>
        );      
    }
    render() {  
        const { classes } = this.props; 
        return (    
            <Fragment>
                <AppBar
                    position="fixed" >
                    <Toolbar>
                        <IconButton onClick={this.toggleDrawer(true)} color="inherit" aria-label="Menu">
                            <MenuIcon />
                        </IconButton>  
                        <Typography 
                            variant="title" 
                            color="inherit" 
                            className={classes.alignTitle}
                        >
                            IBM
                        </Typography>
                        <IconButton component={Link} to="/" replace={this.props.location.pathname === "/"} onClick={this.handlehref} color="inherit" aria-label="Menu">
                            <HomeIcon/>
                        </IconButton>   
                    </Toolbar>
                <Drawer open={this.state.isOpen} onClose={this.toggleDrawer(false)}>
                    <Grid
                    container
                    direction="column"
                    justify="flex-start"
                    alignItems="flex-start"
                    className={classes.drawerGrid}
                    >   
                        { this.DrawerOptions() }   
                    </Grid>
                </Drawer>
                </AppBar>
                {    
                    this.props.children
                }
                <Grid 
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    className={classes.footer}
                >
                    <Grid 
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Typography className={classes.footerLinks} variant="caption" gutterBottom>
                            <a href="https://gitlab.com/jonathanGuzman/ibm-recognition">Gitlab</a>
                        </Typography>            
                    </Grid>
                    <Typography variant="caption" gutterBottom align="center">
                        © Copyright 2018 Jonathan Guzman
                    </Typography>
                </Grid>
            </Fragment>     
        )
    }
}

export default withRouter(withStyles(styles)(Layout));

