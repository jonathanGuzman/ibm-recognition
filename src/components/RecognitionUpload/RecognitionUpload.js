import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

const RecognitionUpload = (props) => {
    const { classes } = props;
    return(
    <div>
        <input
        accept="image/*"
        className={classes.input}
        id="outlined-button-file"
        multiple
        type="file"
        onChange={props.changed}
      />
      <label htmlFor="outlined-button-file">
        <Button variant="outlined" component="span" className={classes.button}>
          Upload
        </Button>
      </label>
    </div>
    )
}

export default withStyles(styles)(RecognitionUpload);