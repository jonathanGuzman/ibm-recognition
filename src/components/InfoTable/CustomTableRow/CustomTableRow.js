import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import CustomTableCell from './CustomTableCell/CustomTableCell';
const styles = theme => ({
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});

const CustomTableRow = (props) => {
    const { classes } = props;
    return(
        <TableRow 
            className={classes.row} 
        >
        <CustomTableCell 
            component="th"
            scope="row"
        >
        { 
            props.index + 1
        }
        </CustomTableCell>
        
        <CustomTableCell 
            component="th"
            scope="row"
        >
        {
            props.gender
        }
        </CustomTableCell>
        
        <CustomTableCell 
            component="th"
            scope="row"
        >
        
        {
            props.min+" - "+props.max
        }
        </CustomTableCell>
    </TableRow>
    )
}

export default withStyles(styles)(CustomTableRow);