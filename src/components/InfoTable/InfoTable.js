import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import Paper from '@material-ui/core/Paper';
import TableRow from '@material-ui/core/TableRow';
import CustomTableRow from './CustomTableRow/CustomTableRow'
import CustomTableCell from './CustomTableRow/CustomTableCell/CustomTableCell'

  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  });
const InfoTable = (props) => {
    const { classes } = props; 
  return (
            <Paper className={classes.root}>
                <Table>
                <TableHead>
                    <TableRow>
                        <CustomTableCell>Person</CustomTableCell>
                        <CustomTableCell>Gender</CustomTableCell>
                        <CustomTableCell>Age</CustomTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {props.faces.map((face, index)=>{
                    console.log("face", face, index);
                    return (
                    <CustomTableRow 
                        key={index}
                        index={index}
                        className={classes.row} 
                        gender={face.gender.gender}
                        min={face.age.min}
                        max={face.age.max}
                    />
                    );
                })}
                </TableBody>
            </Table>
            </Paper>
        )
}
export default withStyles(styles)(InfoTable);