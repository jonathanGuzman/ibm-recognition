import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  recognition: {
    position: 'absolute',
    border: '2px solid black',
  },
  wrapper: {
    position: 'relative'
  }
});

const RecognitionImg = (props) => {
    const { classes } = props;
    return(
      <div className={classes.wrapper}>
      {
        props.faces.map((face, index)=>{
          return (
            <div 
              key={index}
              className={classes.recognition} 
              style={{
                left: face.face_location.left,
                top: face.face_location.top,
                width: face.face_location.width,
                height: face.face_location.height
              }}
            >
            </div>
          );
        })
      }
        <img 
          src={props.imageSrc}
          alt="Recognition"
        />
      </div>

    )
}

export default withStyles(styles)(RecognitionImg);